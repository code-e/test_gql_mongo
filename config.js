const config = {
    port: 4040,
    database: {
        mongodb:{
          url: "mongodb://localhost:27017/"
        }
    },
    graphql: {
        endpoint_path: "/api",
        tracing: true,
        playground: {
            settings: {
                "editor.cursorShape": "line",
                "editor.theme": "light",
                "tracing.hideTracingResponse": false
            }
        }
    }
};

module.exports = config;
