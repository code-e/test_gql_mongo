const {gql} = require('apollo-server-express');

const UserSchema = gql`
    type User {
        _id: String!
        name: String!
        email: String!
        createdAt: Date
    }
`;
module.exports = UserSchema;
