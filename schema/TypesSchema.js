const {gql} = require('apollo-server-express');

const CustomTypesSchema = gql`
    scalar Date
    scalar Boolean
    scalar JSON

    type AuthResponse {
        User: User!
    }
    
    type Post{
        _id: String!,
        title: String,
        text: String
    }
`;

module.exports = CustomTypesSchema;