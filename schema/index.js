const fs = require("fs");
const path = require("path");
let schemas = [];

fs.readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js") && (file !== "node_modules");
    })
    .forEach(function (file) {
        let schema = require(path.join(__dirname, file));
        schemas.push(schema);
    });

module.exports = schemas;