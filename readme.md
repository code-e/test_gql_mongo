**How to start**

    docker-compose up -d
    npm install
    npm start
    
    
**How to use**

    Go to http://localhost:4040/api    
    
**Create User**
    
    mutation{
        registration(name:"V", email:"code.2xe@gmail.com"){
            User{
                name
           }
        }
    
**Login User**

    mutation{
        login(email:"code.2xe@gmail.com"){
            User{
              _id,
              name,
              email
            }
        }
    }
    
    
**Create Post**

    mutation{
      createPost(
        title: "new post 1",
        text: "description new post",
        userId: "5c62a3a9a93a9762626f1108"){
        _id,
        text,
        title
      }
    }

**Edit Post**

    mutation{
      editPost(
        _id: "5c62afe215d9971c62481537",
        userId: "5c62a3a9a93a9762626f1108"
        title: "new post title2"
      	text: "new text")
      }
    }

**Delete Post**
    
    mutation{
        deletePost(_id: "5c62afe115d9971c62481536", userId: "5c62a3a9a93a9762626f1108")
    }
    
**Get Post List**

    query{
      postList(sort:"title", order:-1){
        _id
        title,
        text
      }
    }    