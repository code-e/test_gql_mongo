const mongodb = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const config = require('../config');
const url = config.database.mongodb.url;

const create = async (root, {title, text, userId}) => {
    return await MongoClient.connect(url)
        .then(async (db) => {
            var dbo = db.db("mydb");
            return await dbo.collection("Posts")
                .insertOne({title, text, userId})
                .then((data) => {
                    return data.ops[0];
                }).catch(err => {
                    return err;
                })
        });
};

const edit = async (root, {_id, userId, title, text}) => {
    return await MongoClient.connect(url)
        .then(async (db) => {
            const dbo = db.db("mydb");
            const  filter = {
                _id :  new mongodb.ObjectID(_id),
                userId:userId
            };

            let obj = {};
            if(title){
                obj.title = title;
            }
            if(text){
                obj.text = text;
            }

            return await dbo.collection("Posts")
                .updateOne(filter, {$set:obj})
                .then((data) => {
                    return data.result;
                }).catch(err => {
                    return err;
                })
        });
};

const remove = async (root, {_id, userId}) => {
    return await MongoClient.connect(url)
        .then(async (db) => {
            var dbo = db.db("mydb");
            return await dbo.collection("Posts")
                .deleteOne({
                    _id :  new mongodb.ObjectID(_id),
                    userId: userId
                })
                .then((data) => {
                    console.log(data.deletedCount);
                    return data.result;
                }).catch(err => {
                    return err;
                })
        });
};

const list = async (root, {sort, order}) => {
    const _sort = {};
    _sort[sort || "_id"] = order || 1;

    return await MongoClient.connect(url)
        .then(async (db) => {
            var dbo = db.db("mydb");
            return await dbo.collection("Posts")
                .find({}).sort(_sort).toArray()
                .then((data) => {
                    return data;
                }).catch(err => {
                    return err;
                })
        });
};




module.exports = {
    create, edit, remove, list
};
