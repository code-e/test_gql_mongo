const config = require('./config');
const {ApolloServer} = require('apollo-server-express');
const express = require('express');
const app = express();
const schemas = require('./schema');
const user_resolver = require('./resolver/user_resolver');
const post_resolver = require('./resolver/post_resolver');
const log4js = require('log4js'); // include log4js

log4js.configure({
    appenders: { cheese: { type: 'file', filename: './logs/log.log' } },
    categories: { default: { appenders: ['cheese'], level: 'ALL' } }
});

const logger = log4js.getLogger('cheese');


const resolvers = {
    Query: {
        postList: post_resolver.list
    },

    Mutation: {
        registration: user_resolver.registration,
        login: user_resolver.login,
        createPost: post_resolver.create,
        editPost: post_resolver.edit,
        deletePost: post_resolver.remove,
    }
};

const server = new ApolloServer({
    typeDefs: schemas,
    resolvers: resolvers,
    tracing: true,
    debug: true,
    context: ({req}) => {
        const obj = {
            "addr": req.connection.remoteAddress,
            "query": req.body.query
        };
        logger.info(JSON.stringify(obj));
    },

    formatError: error => {
        logger.error(JSON.stringify(error));
        return error;
    },
    playground: config.graphql.playground
});

server.applyMiddleware({app: app, path: "/api"});

(async () => {
    app.listen({port: config.port}, () => {
        console.info(`\n_/﹋\\_\n(҂\`_´)\n<,︻╦╤─ ҉ Server ready at ${config.port} port\n_/﹋\\_`);
    });
})();