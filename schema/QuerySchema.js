const {gql} = require('apollo-server-express');

const QuerySchema = gql`    
    type Query {
        postList(
            sort: String
            order: Int
        ): [Post]
    }

    type Mutation {
        "registration user"
        registration (
            name: String!
            email: String!
        ): AuthResponse
        
        login (
            email: String!
        ): AuthResponse
        
        createPost(
            title: String!, 
            text: String, 
            userId: String!,
        ): Post
        
        editPost(
            _id: String!, 
            title: String,
            userId: String!,
            text: String
        ): JSON
        
        deletePost(
            _id: String!,
            userId: String!  
        ): JSON
    }
`;

module.exports = QuerySchema;