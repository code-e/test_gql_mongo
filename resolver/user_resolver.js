const MongoClient = require('mongodb').MongoClient;
const config = require('../config');
const url = config.database.mongodb.url;

const login = async (root, {email}) => {
    return await MongoClient.connect(url)
        .then(async (db) => {
            var dbo = db.db("mydb");
            return await dbo.collection("Users")
                .findOne({email:email})
                .then((data) => {
                    return {User:data};
                }).catch(err => {
                    return err;
                })
        });
};

const registration = async (root, {name, email}) => {
    console.log(name, email);
    return await MongoClient.connect(url)
        .then(async (db) => {
            var dbo = db.db("mydb");
            dbo.collection("Users").createIndex( { "email": 1 }, { unique: true } );
            var myobj = {name, email};
            return await dbo.collection("Users")
                .insertOne(myobj)
                .then((data) => {
                    return {User:data.ops[0]};
                }).catch(err => {
                    return err;
                })
        });
};

module.exports = {
    login, registration
};
